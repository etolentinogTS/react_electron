The base of project is create-react-app

of course you can only execute the react project in browser by `npm start`

For run de aplication in electron with dinamic content of react you need:

1 . `git clone https://etolentinogTS@bitbucket.org/etolentinogTS/react_electron.git`

2 . `cd react_electron`

3 . `npm run dev`

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
